#!/usr/bin/env python
# Proof of concent to export documents from ClickUp
# Fill the configurable part with your settings and run this file.
import asyncio
import requests
import json
import random
import string
from websockets import client

### Begin of configurable part ###

# Open the document to export and extract these from URL:
# https://app.clickup.com/{TEAM_ID}/v/dc/{SPACE_ID/{DOCUMENT_ID}
DOCUMENT_ID = ""
TEAM_ID = ""

# Extract from the headers of any authenticated request sent to ClickUp:
# Authorization: Bearer {TOKEN}
TOKEN = ""

# Any of the supported exports: markdown, pdf, html
EXPORT_TYPE = ""

### End of configurable part ###

SESSION_ID = ''.join(random.choice(string.ascii_lowercase) for i in range(10))

async def send_ws(uri):
    async with client.connect(uri, open_timeout=None) as ws:
        auth_msg = f"""
{{
    "method":"auth",
    "token":"{TOKEN}",
    "sessionId":"{SESSION_ID}",
    "teamId":"{TEAM_ID}",
    "notifsForAllTeams":true,
    "buildVersion":"2.170.0"
}}
"""
        print("Sending auth message...")
        await ws.send(auth_msg)
        print("... done")
        print("Waiting for reply...")
        while True:
            data = await ws.recv()
            parsed_data = json.loads(data)
            method = parsed_data.get("method", None)
            msg = parsed_data.get("msg", None)
            if method == None:
                if msg == "AuthReceived":
                    print("... authentication done")
                    print("Sending request to export...")
                    post_headers = {
                        'Authorization': f"Bearer {TOKEN}",
                        'sessionId': SESSION_ID,
                    }
                    response = requests.post(f"https://app.clickup.com/v1/doc/{DOCUMENT_ID}/{EXPORT_TYPE}", json={}, headers=post_headers)
                    if response.status_code != 200:
                        raise "Unexpected response "
                    print("... done")
                    print("Waiting for export to complete...")
                else:
                    print(f"Unknown message: {data}")
            elif method == "ping":
                await ws.send('{"method":"pong"}')
            elif method == "exportDone":
                print(f"... done: {data}")
                break
            elif method == "anyTeamNotificationsAvailable":
                pass
            else:
                print(f"Unknown message: {data}")

asyncio.run(send_ws("wss://ws.clickup.com/ws"))
